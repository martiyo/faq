#+title: El traductor de google en emacs
#+date: <2020-11-27 17:40>
#+filetags: emacs

* Instalación  

  #+BEGIN_EXAMPLE
  M-x package-install RET google-translate
  #+END_EXAMPLE

** Uso 

   Selecionar el texto a traducir y lanzar:

   #+BEGIN_EXAMPLE
   M-x google-translate-at-point
   #+END_EXAMPLE

   Luego indicar el idioma en el que esta el texto y al que hay que traducir.
   Para traducir siempre de inglés a español se agrega al init.el:

   #+BEGIN_EXAMPLE
   (custom-set-variables
   '(google-translate-default-source-language "en")
   '(google-translate-default-target-language "es"))
   #+END_EXAMPLE

*Fuente*

Esto es solo una pequeñisima parte del [[http://lapipaplena.duckdns.org/emacs/][curso de emacs]] de [[https://lapipaplena.wordpress.com/][la pipa plena]]
